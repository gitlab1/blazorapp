﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorApp.Entities
{
    public partial class TbJobTitle
    {
        public TbJobTitle()
        {
            TbProduct = new HashSet<TbProduct>();
        }

        [Key]
        [Column("JobTitleID")]
        public int JobTitleId { get; set; }
        [Required]
        [StringLength(255)]
        public string JobTitleName { get; set; }

        [InverseProperty("JobTitle")]
        public virtual ICollection<TbProduct> TbProduct { get; set; }
    }
}
