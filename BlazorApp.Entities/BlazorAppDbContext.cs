﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BlazorApp.Entities
{
    public partial class BlazorAppDbContext : DbContext
    {
        
        public BlazorAppDbContext(DbContextOptions<BlazorAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbJobTitle> TbJobTitle { get; set; }
        public virtual DbSet<TbProduct> TbProduct { get; set; }
        public virtual DbSet<TbProductDetail> TbProductDetail { get; set; }
        public virtual DbSet<TbPurchase> TbPurchase { get; set; }
        public virtual DbSet<TbUser> TbUser { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbJobTitle>(entity =>
            {
                entity.HasKey(e => e.JobTitleId)
                    .HasName("PK_JobTitle");

                entity.Property(e => e.JobTitleId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PK_Product");

                entity.Property(e => e.ProductId).UseIdentityAlwaysColumn();

                entity.HasOne(d => d.JobTitle)
                    .WithMany(p => p.TbProduct)
                    .HasForeignKey(d => d.JobTitleId)
                    .HasConstraintName("FK_Product_JobTitle");
            });

            modelBuilder.Entity<TbProductDetail>(entity =>
            {
                entity.HasKey(e => e.ProductDetailId)
                    .HasName("PK_ProductDetail");

                entity.Property(e => e.ProductDetailId).UseIdentityAlwaysColumn();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TbProductDetail)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductDetail");
            });

            modelBuilder.Entity<TbPurchase>(entity =>
            {
                entity.HasKey(e => e.PurchaseId)
                    .HasName("PK_Purchase");

                entity.Property(e => e.PurchaseId).UseIdentityAlwaysColumn();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchase_Product");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchase_User");
            });

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_User");

                entity.HasIndex(e => e.ActivationLink)
                    .HasName("TbUser_ActivationLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Email)
                    .HasName("TbUser_Email_key")
                    .IsUnique();

                entity.HasIndex(e => e.ForgotPasswordLink)
                    .HasName("TbUser_ForgotPasswordLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("TbUser_Username_key")
                    .IsUnique();

                entity.Property(e => e.UserId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
