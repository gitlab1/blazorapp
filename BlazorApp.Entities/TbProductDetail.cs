﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorApp.Entities
{
    public partial class TbProductDetail
    {
        [Key]
        [Column("ProductDetailID")]
        public int ProductDetailId { get; set; }
        [Column("ProductID")]
        public int ProductId { get; set; }
        [Required]
        [StringLength(255)]
        public string Note { get; set; }
        [Column(TypeName = "numeric")]
        public decimal Weight { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty(nameof(TbProduct.TbProductDetail))]
        public virtual TbProduct Product { get; set; }
    }
}
