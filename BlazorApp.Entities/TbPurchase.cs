﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorApp.Entities
{
    public partial class TbPurchase
    {
        [Key]
        [Column("PurchaseID")]
        public int PurchaseId { get; set; }
        [Column("UserID")]
        public int UserId { get; set; }
        [Column("ProductID")]
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime PurchaseDate { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty(nameof(TbProduct.TbPurchase))]
        public virtual TbProduct Product { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(TbUser.TbPurchase))]
        public virtual TbUser User { get; set; }
    }
}
