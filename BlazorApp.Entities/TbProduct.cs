﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorApp.Entities
{
    public partial class TbProduct
    {
        public TbProduct()
        {
            TbProductDetail = new HashSet<TbProductDetail>();
            TbPurchase = new HashSet<TbPurchase>();
        }

        [Key]
        [Column("ProductID")]
        public int ProductId { get; set; }
        [Required]
        [StringLength(255)]
        public string ProductName { get; set; }
        [Column(TypeName = "numeric")]
        public decimal Price { get; set; }
        public int Stock { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(255)]
        public string PasswordUser { get; set; }
        public bool? Gender { get; set; }
        [Column("JobTitleID")]
        public int? JobTitleId { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime? TanggalBeli { get; set; }

        [ForeignKey(nameof(JobTitleId))]
        [InverseProperty(nameof(TbJobTitle.TbProduct))]
        public virtual TbJobTitle JobTitle { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<TbProductDetail> TbProductDetail { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
    }
}
