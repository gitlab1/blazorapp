CREATE TABLE "TbJobTitle"
	(
	"JobTitleID"   INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"JobTitleName" VARCHAR (255) NOT NULL,
	CONSTRAINT "PK_JobTitle" PRIMARY KEY ("JobTitleID")
	);

CREATE TABLE "TbProduct"
	(
	"ProductID"   INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"ProductName" VARCHAR (255) NOT NULL,
	"Price"       DECIMAL NOT NULL,
	"Stock"       INT NOT NULL,
	"JobTitleID" INT,
	"TanggalBeli" TIMESTAMPTZ,
	CONSTRAINT "PK_Product" PRIMARY KEY ("ProductID"),
	CONSTRAINT "FK_Product_JobTitle" FOREIGN KEY ("JobTitleID") REFERENCES "TbJobTitle" ("JobTitleID")
	);
	
CREATE TABLE "TbProductDetail"
	(
	"ProductDetailID" INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"ProductID"   INT NOT NULL,
	"Note" VARCHAR (255) NOT NULL,
	"Weight"       DECIMAL NOT NULL,
	CONSTRAINT "PK_ProductDetail" PRIMARY KEY ("ProductDetailID"),
	CONSTRAINT "FK_Product_ProductDetail" FOREIGN KEY ("ProductID") REFERENCES "TbProduct" ("ProductID")
	);	

CREATE TABLE "TbUser"
	(
	"UserID"             INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"Username"           VARCHAR (20) NOT NULL UNIQUE,
	"PasswordUser"       VARCHAR(255) NOT NULL,
    "RoleUser"           VARCHAR(20)  NOT NULL,
	"Email"              VARCHAR(255) NOT NULL UNIQUE,
	"IsActive"           BOOLEAN      NOT NULL DEFAULT FALSE,
	"ActivationLink"     VARCHAR(255) NOT NULL UNIQUE,
	"ForgotPasswordLink" VARCHAR(255) UNIQUE,
	CONSTRAINT "PK_User" PRIMARY KEY ("UserID")
	);

	CREATE TABLE "TbPurchase"
	(
	"PurchaseID"   INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"UserID"       INT NOT NULL,
	"ProductID"    INT NOT NULL,
	"Quantity"     INT NOT NULL,
	"PurchaseDate" TIMESTAMPTZ NOT NULL,
	CONSTRAINT "PK_Purchase" PRIMARY KEY ("PurchaseID"),
	CONSTRAINT "FK_Purchase_User" FOREIGN KEY ("UserID") REFERENCES "TbUser" ("UserID"),
	CONSTRAINT "FK_Purchase_Product" FOREIGN KEY ("ProductID") REFERENCES "TbProduct" ("ProductID")
	);