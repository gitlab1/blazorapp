﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorApp.Entities
{
    public partial class TbUser
    {
        public TbUser()
        {
            TbPurchase = new HashSet<TbPurchase>();
        }

        [Key]
        [Column("UserID")]
        public int UserId { get; set; }
        [Required]
        [StringLength(20)]
        public string Username { get; set; }
        [Required]
        [StringLength(255)]
        public string PasswordUser { get; set; }
        [Required]
        [StringLength(20)]
        public string RoleUser { get; set; }
        [Required]
        [StringLength(255)]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string ActivationLink { get; set; }
        [StringLength(255)]
        public string ForgotPasswordLink { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
    }
}
