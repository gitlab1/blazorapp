﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class ProductDetailListItemViewModel
    {
        public int ProductDetailID { get; set; }
        public int ProductID { get; set; }
        public string Note { set; get; }
        public decimal Weight { get; set; }
    }
}
