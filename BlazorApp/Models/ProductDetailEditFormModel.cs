﻿using Accelist.BlazorRAD;
using BlazorApp.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class ProductDetailEditFormModel
    {
        public int ProductDetailID { get; set; }
        public int ProductID { get; set; }
        [SmartFormInput(SmartFormInputType.Text, Row = 1)]
        public string Note { set; get; }
        [SmartFormInput(SmartFormInputType.Number, Row = 2)]
        public decimal Weight { get; set; }
    }

    public class ProductDetailEditFormModelValidator : AbstractValidator<ProductDetailEditFormModel>
    {
        public BlazorAppDbContext DB { get; }

        public ProductDetailEditFormModelValidator(BlazorAppDbContext blazorAppDbContext)
        {
            RuleFor(Q => Q.Note).NotEmpty().MinimumLength(3).MaximumLength(255);
            RuleFor(Q => Q.Weight).NotEmpty();
            DB = blazorAppDbContext;
        }
    }
}
