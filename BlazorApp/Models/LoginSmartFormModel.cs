﻿using Accelist.BlazorRAD;
using BlazorApp.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class LoginSmartFormModel
    {
        [SmartFormInput(SmartFormInputType.Text, Row = 1)]
        public string UserName { set; get; }
        [SmartFormInput(SmartFormInputType.Password, Row = 2)]
        public string Password { set; get; }
    }

    public class LoginSmartFormModelValidator : AbstractValidator<LoginSmartFormModel>
    {
        public BlazorAppDbContext DB { get; }

        public LoginSmartFormModelValidator(BlazorAppDbContext blazorAppDbContext)
        {
            RuleFor(Q => Q.UserName).NotEmpty().MinimumLength(3).MaximumLength(255);
            RuleFor(Q => Q.Password).NotEmpty().MinimumLength(8).MaximumLength(64);
            DB = blazorAppDbContext;
        }
    }
}
