﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class ProductListItemViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { set; get; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}
