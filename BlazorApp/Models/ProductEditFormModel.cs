﻿using Accelist.BlazorRAD;
using BlazorApp.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class ProductEditFormModel
    {
        public int ProductId { get; set; }
        [SmartFormInput(SmartFormInputType.Text, Row = 1)]
        public string ProductName { set; get; }
        [SmartFormInput(SmartFormInputType.Number, Row = 2)]
        public decimal Price { get; set; }
        [SmartFormInput(SmartFormInputType.Number, Row = 3)]
        public int Stock { get; set; }
        [SmartFormInput(SmartFormInputType.Autocomplete, Row = 4)]
        public SelectOption JobTitle { set; get; }
    }

    public class ProductEditFormModelValidator : AbstractValidator<ProductEditFormModel>
    {
        public BlazorAppDbContext DB { get; }

        public ProductEditFormModelValidator(BlazorAppDbContext blazorAppDbContext)
        {
            RuleFor(Q => Q.ProductName).NotEmpty().MinimumLength(3).MaximumLength(255);
            RuleFor(Q => Q.Price).NotEmpty();
            RuleFor(Q => Q.Stock).NotEmpty();
            //RuleFor(Q => Q.Email).NotEmpty().MaximumLength(255).EmailAddress()
            //    .MustAsync(EmailNotTaken).WithMessage("{PropertyName} has already been registered with another account!");
            //RuleFor(Q => Q.Password).NotEmpty().MinimumLength(8).MaximumLength(64);
            //RuleFor(Q => Q.PasswordVerify).NotEmpty().Equal(Q => Q.Password);
            RuleFor(Q => Q.JobTitle).NotEmpty();
            DB = blazorAppDbContext;
        }

        //public async Task<bool> EmailNotTaken(string email, CancellationToken cancellationToken)
        //{
        //    var exist = await DB.Employees
        //        .AsNoTracking()
        //        .Where(Q => Q.Email == email)
        //        .AnyAsync(cancellationToken);
        //    return (exist == false);
        //}
    }
}
