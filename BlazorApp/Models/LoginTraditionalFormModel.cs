﻿using Accelist.BlazorRAD;
using BlazorApp.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class LoginTraditionalFormModel
    {
        public string UserName { set; get; }
        public string Password { set; get; }
    }

    public class LoginTraditionalFormModelValidator : AbstractValidator<LoginTraditionalFormModel>
    {
        public BlazorAppDbContext DB { get; }

        public LoginTraditionalFormModelValidator(BlazorAppDbContext blazorAppDbContext)
        {
            RuleFor(Q => Q.UserName).NotEmpty().MinimumLength(3).MaximumLength(255);
            RuleFor(Q => Q.Password).NotEmpty().MinimumLength(8).MaximumLength(64);
            DB = blazorAppDbContext;
        }
    }
}
