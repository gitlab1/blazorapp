﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class LoginDbModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string PasswordUser { get; set; }
        public string RoleUser { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
    }
}
