using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accelist.BlazorRAD;
using BlazorApp.Entities;
using BlazorApp.Enums;
using BlazorApp.Services;
using FluentValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
//using BlazorApp.Data;

namespace BlazorApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            //services.AddControllers(); // Bisa pake Web API
            services.AddServerSideBlazor();
            //services.AddSingleton<WeatherForecastService>();

            // Scoped: componentnya unique per request
            services.AddScoped<ScopedServices>();
            services.AddTransient<BlazorAppService>();

            services.AddDbContextPool<BlazorAppDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostGreConnString"), strategy =>
                {
                    strategy.EnableRetryOnFailure();
                }
                );
            });

            //services.AddScoped<IValidator<EmployeeCreateForm>, EmployeeCreateFormValidator>();
            services.AddValidatorsFromAssemblyContaining<Program>();

            //services.AddAuthentication(BlazorAppAuthenticationSchemes.Cookie)
            //        .AddCookie(BlazorAppAuthenticationSchemes.Cookie, options =>
            //        {
            //            options.LoginPath = "/Auth/Login";
            //            options.LogoutPath = "/Auth/Logout";
            //            options.AccessDeniedPath = "/Auth/AccessDenied";

            //            options.Cookie.IsEssential = true;
            //            options.Cookie.HttpOnly = true;
            //            options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            //        })
            ////    .AddScheme<AuthenticationSchemeOptions, JoseAuthenticationHandler>(TrainingAuthenticationSchemes.Token, options => { })
            //    ;

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "cookie";
                options.DefaultChallengeScheme = "oidc";
            }).AddCookie("cookie").AddOpenIdConnect("oidc", options =>
            {
                options.Authority = "https://sso-dev.accelist.com";
                options.ClientId = "mikhael_auth";
                options.ResponseType = "code";
                options.UsePkce = true;
                options.GetClaimsFromUserInfoEndpoint = true;
                options.Scope.Add("email");
                options.Scope.Add("roles");
                options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "sub");
                options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
                options.ClaimActions.MapJsonKey(ClaimTypes.Role, "roles");
            });

            services.AddScoped<SweetAlertJSInterop>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapRazorPages();
                endpoints.MapBlazorHub();
                //endpoints.MapDefaultControllerRoute(); // Bisa pake Web API
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
