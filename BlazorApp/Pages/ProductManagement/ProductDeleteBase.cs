﻿using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Pages.ProductManagement
{
    public class ProductDeleteBase : ScopedServicesComponent
    {
        public bool Submitting { set; get; } = false;

        public string SuccessMessage { set; get; }

        public ProductDeleteFormModel Form { set; get; } = new ProductDeleteFormModel();

        /// <summary>
        /// buat nyari dan nampung datanya kyk yang edit
        /// </summary>
        //protected override void OnInitialized()
        protected override async Task OnInitializedAsync()
        {
            var deleting = await Service.DB.TbProduct
            .Where(Q => Q.ProductId == Form.ProductId)
            .FirstOrDefaultAsync();

            if (deleting != null)
            {
                Form.ProductId = deleting.ProductId;
                Form.ProductName = deleting.ProductName;
                Form.Price = deleting.Price;
                Form.Stock = deleting.Stock;
            }
            //Ready = true;
        }

        public async Task OnSubmit()
        {
            Submitting = true;
            try
            {
                this.Service.DB.TbProduct.Add(new TbProduct
                {
                    ProductName = Form.ProductName,
                    Price = Form.Price,
                    Stock = Form.Stock
                });

                await this.Service.DB.SaveChangesAsync();
                SuccessMessage = $"Successfully deleted product: {Form.ProductName}";
            }
            finally
            {
                Submitting = false;
            }

            Form = new ProductDeleteFormModel();
        }

        public async Task<bool> DeleteProduct()
        {
            var product = await this.Service.DB
                .TbProduct
                .Where(Q => Q.ProductId == Form.ProductId)
                .FirstOrDefaultAsync();

            if (product == null)
            {
                return false;
            }
            this.Service.DB.Remove(product);
            await this.Service.DB.SaveChangesAsync();
            return true;
        }
    }
}
