﻿using Accelist.BlazorRAD;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Pages.ProductManagement
{
    public class ProductCreateBase : ScopedServicesComponent
    {
        //private readonly BlazorAppService _blazorAppMan;
        //public ProductCreateBase(BlazorAppService blazorAppService)
        //{
        //    this._blazorAppMan = blazorAppService;
        //}

        public bool Submitting { set; get; } = false;

        public string SuccessMessage { set; get; }

        public ProductCreateFormModel Form { set; get; } = new ProductCreateFormModel();

        public SelectOptionMethods SelectOptions = new SelectOptionMethods();

        protected override void OnInitialized()
        {
            SelectOptions[nameof(ProductCreateFormModel.Gender)] = GenderSelectOptions;
            SelectOptions[nameof(ProductCreateFormModel.JobTitle)] = JobTitleSelectOptions;
        }

        private Task<List<SelectOption>> GenderSelectOptions(string value)
        {
            var genders = new List<SelectOption>();
            genders.Add(new SelectOption
            {
                Name = "Male",
                Value = "Male"
            });
            genders.Add(new SelectOption
            {
                Name = "Female",
                Value = "Female"
            });
            return Task.FromResult(genders);
        }

        private Task<List<SelectOption>> JobTitleSelectOptions(string value)
        {
            var query = Service.DB.TbJobTitle.AsNoTracking();

            if (string.IsNullOrWhiteSpace(value) == false)
            {
                var search = value.Trim() + "%"; // starts with
                query = query.Where(Q => EF.Functions.Like(Q.JobTitleName, search));
            }

            return query.Select(Q => new SelectOption
            {
                Name = Q.JobTitleName,
                Value = Q.JobTitleId.ToString()
            }).ToListAsync();
        }

        public async Task OnSubmit()
        {
            Submitting = true;
            try
            {
                var jenisKelamin = true; //Male
                if (Form.Gender == "Female")
                {
                    jenisKelamin = false; //Female
                }
                
                this.Service.DB.TbProduct.Add(new TbProduct
                {
                    ProductName = Form.ProductName,
                    Price = Form.Price,
                    Stock = Form.Stock,
                    JobTitleId = int.Parse(Form.JobTitle.Value),
                    Gender = jenisKelamin
                });

                await this.Service.DB.SaveChangesAsync();
                SuccessMessage = $"Successfully created new product: {Form.ProductName}";
            }
            finally
            {
                Submitting = false;
            }

            Form = new ProductCreateFormModel();
        }
    }
}
