using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BlazorApp.Enums;
using BlazorApp.Models;
using BlazorApp.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BlazorApp.Pages.Auth
{
    public class LoginModel : PageModel
    {
        //public IActionResult OnGet(string returnUrl)
        public IActionResult OnGet()
        {
            return Challenge(new AuthenticationProperties
            {
                //RedirectUri = returnUrl
                RedirectUri = "http://localhost:63153/Product"
            }) ;
        }
    }
}
