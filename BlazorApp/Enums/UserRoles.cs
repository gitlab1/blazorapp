﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Enums
{
    public class UserRoles
    {
        public const string User = "User";

        public const string Admin = "Admin";    
    }
}
