﻿using BlazorApp.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/aspnet/core/blazor/dependency-injection?view=aspnetcore-3.1#utility-base-component-classes-to-manage-a-di-scope
    /// </summary>
    public class ScopedServicesComponent : OwningComponentBase<ScopedServices>
    {
    }
}
