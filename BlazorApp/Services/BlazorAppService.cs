﻿using BlazorApp.Entities;
using BlazorApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    public class BlazorAppService
    {
        private readonly BlazorAppDbContext _blazorAppDbContext;

        public ProductDetailCreateFormModel ProductDetailCreateForm { get; set; } = new ProductDetailCreateFormModel();

        public BlazorAppService(BlazorAppDbContext dbContext)
        {
            this._blazorAppDbContext = dbContext;
        }

        public bool Verify(string notHash, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(notHash, hash);
        }

        public async Task<LoginDbModel> GetLogin(string username)
        {
            var getData = await this._blazorAppDbContext.TbUser
                .Where(Q => Q.Username == username)
                .Select(Q => new LoginDbModel
                {
                    UserId = Q.UserId,
                    Username = Q.Username,
                    PasswordUser = Q.PasswordUser,
                    RoleUser = Q.RoleUser,
                    Email = Q.Email,
                    IsActive = Q.IsActive
                }).FirstOrDefaultAsync();
            return getData;
        }

        public void ProductDetailResetCreateForm()
        {
            ProductDetailCreateForm = new ProductDetailCreateFormModel();
        }

        public async Task ProductDetailSubmitCreateForm()
        {
            // emulate network delay for demo purposes only
#if DEBUG 
            await Task.Delay(3_000);
#endif

            var productDetail = new TbProductDetail
            {

                Note = ProductDetailCreateForm.Note,
                Weight = ProductDetailCreateForm.Weight
            };

            _blazorAppDbContext.Add(productDetail);
            
            await _blazorAppDbContext.SaveChangesAsync();
        }
    }
}
