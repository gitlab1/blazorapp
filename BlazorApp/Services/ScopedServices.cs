﻿using BlazorApp.Entities;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    public class ScopedServices
    {
        public ScopedServices(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public IServiceProvider ServiceProvider { get; }

        public T Svc<T>()
        {
            return ServiceProvider.GetRequiredService<T>();
        }

        //public MyScopedService ScopedService => Svc<MyScopedService>();

        public BlazorAppDbContext DB => Svc<BlazorAppDbContext>();
        public BlazorAppService ProductDetailCRUD => Svc<BlazorAppService>();


    }
}
